# ffmpeg install with library

### MacOs
```bash

brew reinstall ffmpeg 


brew install ffmpeg --with-fdk-aac --with-ffplay --with-freetype --with-frei0r --with-libass --with-libvo-aacenc --with-libvorbis --with-libvpx --with-opencore-amr --with-openjpeg --with-opus --with-rtmpdump --with-schroedinger --with-speex --with-theora --with-tools --enable-libfreetype --enable-libfontconfig --enable-libfribidi --enable-libfreetype --enable-libfreetype  --enable-libfdk-aac --enable-nonfree --enable-gpl
```

### Ubuntu
```bash
sudo apt-get update -qq && sudo apt-get -y install \
  autoconf \
  automake \
  build-essential \
  cmake \
  git-core \
  libass-dev \
  libfreetype6-dev \
  libsdl2-dev \
  libtool \
  libva-dev \
  libvdpau-dev \
  libvorbis-dev \
  libxcb1-dev \
  libxcb-shm0-dev \
  libxcb-xfixes0-dev \
  pkg-config \
  texinfo \
  wget \
  zlib1g-dev

https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu

```



# CELERY INSTALL

```bash
sudo apt-get install rabbitmq-server
```
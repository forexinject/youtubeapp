# How generate auth api credentials.

* Go to the Google [console](https://console.developers.google.com/).
* _Create project_.
* Side menu: _APIs & auth_ -> _APIs_
* Top menu: _Enabled API(s)_: Enable all Youtube APIs.
* Side menu: _APIs & auth_ -> _Credentials_.
* _Create a Client ID_: Add credentials -> OAuth 2.0 Client ID -> Other -> Name: youtube-upload -> Create -> OK
* _Download JSON_: Under the section "OAuth 2.0 client IDs". Save the file to your local system. 
* Use this JSON as your credentials file: ```--client-secrets=CLIENT_SECRETS```



## Notes for developers

- library : https://github.com/tokland/youtube-upload
- Main logic of the upload: main.py (function upload_video).
- Check the (Youtube Data API)[https://developers.google.com/youtube/v3/docs/].
- Some Youtube API (examples)[https://github.com/youtube/api-samples/tree/master/python] provided by Google.

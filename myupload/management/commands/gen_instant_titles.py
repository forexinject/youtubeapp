# coding=utf-8
import os
from django.core.management.base import BaseCommand, CommandError
#from djcoin.app.controllers.cronjobs import _clean_old_files
from celery import shared_task
from django.conf import settings
from myupload.models import VideosModel, TitleVideoModel
from myupload.controllers.google_instant_suggestion import get_instant_suggestions

class Command(BaseCommand):

    def handle(self, *args, **options):

        for video in VideosModel.objects.filter(title_gen=False):
            titles = get_instant_suggestions(video.Title)
            
            if len(titles) == 0:
                return
            for title in titles: #[:50]:

                t = TitleVideoModel(title=title, video_id=video.id, publicato=False)
                t.save()

            video.title_gen = True
            video.save()

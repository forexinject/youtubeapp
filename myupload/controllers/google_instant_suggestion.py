# -*- coding: utf-8 -*-
import requests
import string
import time
import sys
from celery.decorators import task
from celery.utils.log import get_task_logger


headers = {
    'pragma': 'no-cache',
    'accept-encoding': 'gzip, deflate, sdch',
    'accept-language': 'en-EN,en;q=0.8',
    'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36',
    'accept': '*/*',
    'cache-control': 'no-cache',
    'authority': 'www.google.co.uk',
    'referer': 'https://www.google.co.uk/',
}

logger = get_task_logger(__name__)


@task(name="generate_google_suggestions")
def get_instant_suggestions(keyword):
    logger.info("start generate suggestions by celery")
    db_keywords_uniq = set()
    for line in  list(string.ascii_lowercase):
        time.sleep(1)
        try:
            params = (
                ('client', 'psy-ab'),
                ('hl', 'en'),
                ('gs_rn', '64'),
                ('gs_ri', 'psy-ab'),
                ('cp', '5'),
                ('gs_id', '1h'),
                #('q', line + " " + keyword),
                ('q',  keyword + " " + line),
                ('xhr', 't'),
            )

            r = response = requests.get('https://www.google.co.uk/complete/search', headers=headers, params=params)
            # print(r.json())

            for line in r.json()[1]:
                text =  line[0].replace("</b>","").replace("<b>","")
                # print(text)

                db_keywords_uniq.add(text.replace("\n"," "))

        except Exception as error:
            print(error)
            #return []

    return db_keywords_uniq

from django_cron import CronJobBase, Schedule
from myupload.models import VideosModel, TitleVideoModel
from myupload.controllers.google_instant_suggestion import get_instant_suggestions


def auto_generate_titles():
    video =VideosModel.objects.filter(title_gen=False).first()
    if video:
        titles = list( get_instant_suggestions(video.Title) )

        for title in titles: #[:50]:
            t = TitleVideoModel(title=title, video_id=video.id, publicato=False)
            print('New title > ', t)
            t.save()

        video.title_gen = True
        video.save()



class TitleJob(CronJobBase):
    RUN_EVERY_MINS =  1
    RETRY_AFTER_FAILURE_MINS = 5

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'videoyoutube.myupload.TitleJob'  # a unique code

    def do(self):
        print("start crontab titlejob")
        auto_generate_titles()

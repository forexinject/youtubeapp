# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin)

from django.db import models
from datetime import datetime
from django.utils.timezone import now
from django.contrib.auth.models import User
import uuid
import os

# Create your models here.


# TODO: save uploaded file with custom name /accounts/{id}/secrets.json

def update_filename(instance, filename):
    path = "accounts/"
    format = str(uuid.uuid4()) + "/" + filename
    return os.path.join(path, format)


class NotDeletedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(deleted=False)


class AccountsModel(models.Model):
    ChannelName = models.CharField(max_length=100, unique=False)
    ChannelURL = models.CharField(
        max_length=100, unique=False, null=False, blank=False)
    ClientSecrets = models.FileField(
        upload_to=update_filename, null=False, blank=False)
    RequestToken = models.FileField(
        upload_to=update_filename, default='', null=False, blank=False)
    Note = models.TextField(null=True, blank=True)
    counter = models.IntegerField(default=0)

    author = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    deleted = models.BooleanField(default=False)

    auth_url = models.TextField(default="", null=True, blank=True)
    onetime_code = models.CharField(
        max_length=255,
        default="",
        null=True,
        blank=True)

    objects = NotDeletedManager()
    all_objects = models.Manager()

    class Meta:
        verbose_name = 'Youtube Account'
        verbose_name_plural = 'Youtube Accounts'

    class Meta:
        ordering = ('updated_at',)

    def __unicode__(self):
        return "{} - {}".format(self.author, self.ChannelName)

    def __str__(self):
        return "{} - {}".format(self.author, self.ChannelName)


CATEGORIES = (
    ('2', 'Autos & Vehicles'),
    ('1', 'Film & Animation'),
    ('10', 'Music'),
    ('15', 'Pets & Animals'),
    ('17', 'Sports'),
    ('18', 'Short Movies'),
    ('19', 'Travel & Events'),
    ('20', 'Gaming'),
    ('21', 'Videoblogging'),
    ('22', 'People & Blogs'),
    ('23', 'Comedy'),
    ('24', 'Entertainment'),
    ('25', 'News & Politics'),
    ('26', 'Howto & Style'),
    ('27', 'Education'),
    ('28', 'Science & Technology'),
    ('29', 'Nonprofits & Activism'),
    ('30', 'Movies'),
    ('31', 'Anime/Animation'),
    ('32', 'Action/Adventure'),
    ('33', 'Classics'),
    ('34', 'Comedy'),
    ('35', 'Documentary'),
    ('36', 'Drama'),
    ('37', 'Family'),
    ('38', 'Foreign'),
    ('39', 'Horror'),
    ('40', 'Sci-Fi/Fantasy'),
    ('41', 'Thriller'),
    ('42', 'Shorts'),
    ('43', 'Shows'),
    ('44', 'Trailers')
)


LANGUAGES = (
    ('EN', 'EN'),
    ('IT', 'IT'),
    ('RO', 'RO'),
    ('DE', 'DE'),
    ('FR', 'FR'),
)


class VideosModel(models.Model):

    Title = models.CharField(max_length=100, unique=False,
                             help_text='youtube video title')
    Description = models.TextField(null=True, blank=True)
    Tags = models.CharField(max_length=250, unique=False)
    Category = models.CharField(
        "Category", max_length=32, default='28', choices=CATEGORIES, null=False, blank=False)
    Language = models.CharField(
        "Language", max_length=32, default='EN', choices=LANGUAGES, null=False, blank=False)
    file = models.FileField(
        upload_to='video/', default='aici video', null=True, blank=False)
    Playlist = models.CharField(max_length=100, unique=False)
    Account = models.ForeignKey(
        AccountsModel, related_name='accounts', null=False, blank=False, on_delete=models.PROTECT)

    title_gen = models.BooleanField(default=False)  #  pentru generat titlurile
    author = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    deleted = models.BooleanField(default=False)

    objects = NotDeletedManager()
    all_objects = models.Manager()

    class Meta:
        verbose_name = 'Video'
        verbose_name_plural = 'Videos'

    def __unicode__(self):
        return "{} {}".format(self.id, self.Title)

    def __str__(self):
        return "{}:{}/{}".format(self.id, self.Title, self.Account)


class TitleVideoModel(models.Model):
    title = models.CharField(max_length=100, unique=False,
                             help_text='youtube video title')
    #video_id       = models.ForeignKey(VideosModel, related_name='videos', default=1, null=False, blank=False, on_delete=models.PROTECT)
    video = models.ForeignKey(VideosModel, related_name='video_id',
                              null=False, blank=False, on_delete=models.PROTECT)
    publicato = models.BooleanField(
        default=False, help_text='video has ben published (True | False)')
    pending = models.BooleanField(
        default=False, help_text='video is pengind processing')
    youtube_url = models.CharField(
        max_length=250, default='', null=True, blank=True, unique=False, help_text='youtube_url')

    ip_address = models.CharField(
        max_length=250, default='', null=True, blank=True, unique=False, help_text='ip_address')
    author = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Video Title'
        verbose_name_plural = 'Video Titles'

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

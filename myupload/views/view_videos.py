# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from ..models import (
    VideosModel,
    AccountsModel,
    CATEGORIES, LANGUAGES)
from ..serializers import (
    VideosSerializer,
    VideoCategorySerializer,
    LanguageSerializer,
    VideoUploadSerializer
    # VideoChannelSerializer
)


class VideosList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = VideosModel.objects.all()
    serializer_class = VideosSerializer

    def get(self, request):
        videos = VideosModel.objects.filter(author__id=request.user.pk)
        serializer = VideosSerializer(videos, many=True)
        return Response(serializer.data)


class VideoDetail(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = VideosModel.objects.all()
    serializer_class = VideosSerializer


class VideoChannelView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = VideosModel.objects.all()
    serializer_class = VideosSerializer

    def get(self, request, channel_id):

        videos = VideosModel.objects.filter(
            Account__author=request.user,
            Account__id=channel_id,
        )

        serializer = VideosSerializer(videos, many=True)
        return Response(serializer.data)


class SimpleChoiceList(generics.ListAPIView):
    """

    """
    permission_classes = (AllowAny,)
    serializer_class = None
    _data = None  # tuples from model choices

    def queryset(self):
        pass

    def get(self, request):
        data = [{"id": i, "title": v}
                for i, v in dict(self._data).items()]

        serializer = self.serializer_class(data=data, many=True)
        serializer.is_valid(raise_exception=True)
        # serializer.save()

        return Response(serializer.data)


class VideoCategoryList(SimpleChoiceList):
    serializer_class = VideoCategorySerializer
    _data = CATEGORIES


class LanguageList(SimpleChoiceList):
    serializer_class = LanguageSerializer
    _data = LANGUAGES


class VideoUploadView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = VideosSerializer

    def get(self, request, pk=None):
        if pk is None:
            videos = VideosModel.objects.filter(Account__author=request.user)

            serializer = self.serializer_class(videos, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        try:
            filters = {
                "Account__author": request.user,
            }
            if pk:
                filters['pk'] = pk
            video = VideosModel.objects.get(**filters)

            serializer = self.serializer_class(video, many=False)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except VideosModel.DoesNotExist as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        print("Adding new file")
        """ Adding a new video. """
        serializer = VideoUploadSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)
        else:
            # TODO: Check if Account is athor's account
            video = serializer.save()

            # data = serializer.data
            # title = data.get('Title')
            # description = data.get('Description')
            # # TODO define default category
            # category = data.get('Category', '28')
            # # TODO: define default langiage
            # language = data.get('Language', 'EN')

            # playlist = request.data.get('Playlist', -1)
            # account = data.get('Account', -1)  # TODO: Check it

            # author = request.user  # duplicated because of relations


            try:
                #video = VideosModel.objects.get(pk=videoid)

                # video = VideosModel(
                #     Title=title,
                #     Description=description,
                #     Category=category,
                #     Language=language,
                #     file=file,
                #     Playlist=playlist,
                #     Account=account,
                #     author=author, # ?? why
                #     created_at=datetime.datetime.now(),
                #     updated_at=datetime.datetime.now(),
                # )
                # video.save()
                request.data['id'] = video.pk  # return id
                return Response({"file_id": video.pk}, status=status.HTTP_201_CREATED)
            except VideosModel.DoesNotExist as error:
                print(error)
                return Response(status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        print("delete video {}".format(pk))
        # Check if video owner is requested user
        try:
            video = VideosModel.objects.get(
                Account__author=request.user,
                pk=pk
            )
        except VideosModel.DoesNotExist as e:
            Response(status=status.HTTP_404_NOT_FOUND)

        video_file = video.file.path
        print(video, video_file)
        # delete video record from DB
        video.deleted = True
        video.save()

        # delete file from file system
        # 
        return Response(status=status.HTTP_200_OK)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from django.db.models import Count, Max
import datetime
import time

from ..models import AccountsModel, TitleVideoModel, VideosModel
from ..serializers import (
    AccountsSerializer,
    BashboardSerializer,
    AccountStep2Serializer)

from rest_framework_simplejwt.views import TokenObtainPairView


#------------ accounts

class AccountsList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AccountsSerializer

    def queryset(self, request):
        return AccountsModel.objects.filter(author=request.user)

    def get_(self, request):
        return Response(status=status.HTTP_200_OK)

    #queryset = AccountsModel.objects.all()


class AccountStep2Detail(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AccountStep2Serializer

    def post(self, request, pk):
        serializer = AccountStep2Serializer(data=request.data)
        if serializer.is_valid():
            try:
                account = AccountsModel.objects.get(author=request.user, pk=pk)
            except AccountsModel.DoesNotExist as e:
                return Response(
                    serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

            code = request.data.get('code')
            account.onetime_code = code
            account.save()

            return Response(
                    {"message": "request token created"},
                    status=status.HTTP_200_OK)


class AccountDetail(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AccountsSerializer

    def queryset_(self, request):
        queryset = AccountsModel.objects.filter(author=request.user.pk)
        return queryset

    def get(self, request, pk=None):
        # TODO: get payload and check if id = ok
        accounts = AccountsModel.objects.filter(author__id=request.user.pk)
        serializerAccount = AccountsSerializer(accounts, many=True)

        return Response(
            serializerAccount.data, status=status.HTTP_200_OK)

    def post(self, request):
        """
        Create a new Account / Channel
        """
        serializer = AccountsSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                account = serializer.save()
                account.author = request.user
                account.save()
            except Exception as e:
                return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

        from myupload.tasks import get_token
        #upload_video_task.delay()
        get_token.apply_async(
            (account.ClientSecrets.path, account.id),
            #countdown=10,
            expires=60,
            secret=account.ClientSecrets.path,
            account_id=account.id
        )

        while not account.auth_url:
            # use signals
            time.sleep(1)
            account.refresh_from_db()

        return Response(
            {"id": account.id, "auth_url": account.auth_url},
            status=status.HTTP_201_CREATED)

    def put(self, request, pk):
        return Response(request.data, status=status.HTTP_201_CREATED)

    def delete(self, request, pk):
        print("delete account {}".format(pk))
        # Check if channel owner is requested user
        try:
            account = AccountsModel.objects.get(
                author=request.user,
                pk=pk
            )
        except AccountsModel.DoesNotExist as e:
            Response(status=status.HTTP_404_NOT_FOUND)


        # delete video record from DB
        account.deleted = True
        account.save()

        return Response(status=status.HTTP_200_OK)


class DashboardAPIView(APIView):
    #permission_classes = (IsAuthenticated,)

    def get(self, request, period):
        if period not in ["today", "week", "month"]:
            return Response(
                {"error": "Wrong period param"},
                status=status.HTTP_404_NOT_FOUND
            )

        now = datetime.datetime.now()

        filters = {
            #"author__id": 2, #request.user,
            "created_at__year": now.year,
        }
        if period == "today":
            filters["created_at__month"] = now.month
            filters["created_at__day"] = now.day

        if period == "month":
            filters["created_at__month"] = now.month

        if period == "week":
            day_of_week = now.weekday()
            start_day = now - datetime.timedelta(days=7)
            filters["created_at__gte"] = start_day

        channels = AccountsModel.objects.filter(**filters).values(
            "author__username", 'author__id'
        ).annotate(
            new_channels=Count('id', distinct=True), last_activity=Max('updated_at')
        ).annotate(
            new_videos=Count('accounts__video_id__id', distinct=True)
        ).order_by("author__username")

        videos = AccountsModel.objects.filter(**filters).values(
            "author__username", 'author__id'
        ).annotate(
            new_videos=Count('accounts__video_id__id', distinct=True)
        ).order_by("author__username")


        #print(channels.query)

        # bashboard = BashboardSerializer(data=stat, many=True)

        return Response(
            channels, status=status.HTTP_200_OK)

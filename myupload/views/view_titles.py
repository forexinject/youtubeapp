# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import generics, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from myupload.models import TitleVideoModel, VideosModel, AccountsModel
from myupload.serializers import (
    TitlesSerializer,
    VideosSerializer,
    AccountsSerializer
)
from rest_framework.views import APIView
from rest_framework.response import Response
from datetime import datetime

from myupload.controllers.google_instant_suggestion import (
    get_instant_suggestions
)


#------------ title


class TitlesList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = TitleVideoModel.objects.all()
    serializer_class = TitlesSerializer


class TitleDetail(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = TitleVideoModel.objects.all()
    serializer_class = TitlesSerializer


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class TitleConversion(APIView):
    """ acest api returneaza titlul video pentru convertit si toate datele necesare """

    def get(self, request):
        ip_address = get_client_ip(request).strip()

        # check if allready exist task for this ip not finished and return same
        # task.
        task = TitleVideoModel.objects.filter(
            pending=True, publicato=False, ip_address=ip_address).first()
        if task:
            video = VideosModel.objects.filter(id=task.video_id).first()
            if not video:
                return Response({'status': 'nu gasesc video for title_id : {}'.format(task.id)},
                                status=status.HTTP_404_NOT_FOUND)

            account = AccountsModel.objects.filter(id=video.Account_id).first()
            if not account:
                return Response({'status': 'nu gasesc account for video_id : {}'.format(video.id)},
                                status=status.HTTP_404_NOT_FOUND)

            serializerVideo = VideosSerializer(video, many=False)
            serializerTitle = TitlesSerializer(task, many=False)
            serializerAccount = AccountsSerializer(account, many=False)

            return Response({"title":  serializerTitle.data,
                             'video':   serializerVideo.data,
                             'account': serializerAccount.data,
                             "task": "old"})

        # get new task for worker.
        task = TitleVideoModel.objects.filter(
            pending=False, video__isnull=False, publicato=False).first()
        if task:
            video = VideosModel.objects.filter(id=task.video_id).first()
            if not video:
                return Response({'status': 'nu gasesc video for title_id : {}'.format(task.id)},
                                status=status.HTTP_404_NOT_FOUND)

            account = AccountsModel.objects.filter(id=video.Account_id).first()
            if not account:
                return Response({'status': 'nu gasesc account for video_id : {}'.format(video.id)},
                                status=status.HTTP_404_NOT_FOUND)

            serializerVideo = VideosSerializer(video, many=False)
            serializerTitle = TitlesSerializer(task, many=False)
            serializerAccount = AccountsSerializer(account, many=False)

            task.ip_address = get_client_ip(request)
            task.pending = True
            task.save()
            return Response({"title": serializerTitle.data,
                             'video': serializerVideo.data,
                             'account': serializerAccount.data,
                             "task": "new"})

        return Response({'message': 'nu gasesc nici un titlul disponibil pentru conversie'},
                        status=status.HTTP_404_NOT_FOUND)


class TitleAsDone(APIView):
    # permission_classes = (IsAuthenticated,)
    """ this api mark as done published on youtube title """

    def post(self, request):

        # todo: neeed sterilize data

        title = TitleVideoModel.objects.get(pk=request.data.get('title_id'))
        if title:
            title.publicato = True
            title.youtube_url = request.data.get('youtube_url', None)
            title.save()

            # increment counter of video published with account.
            account = AccountsModel.objects.get(id=title.video.Account_id)
            account.counter += 1
            account.save()

            #print("debug", title.id)

            return Response(request.data, status=status.HTTP_201_CREATED)

        return Response(status=status.HTTP_404_NOT_FOUND)


class TitlesView(APIView):
    permission_classes = (IsAuthenticated,)
    """ List of video title ready for publish on youtube """

    def get(self, request, title_id=None):
        print(title_id)
        """ Get all todos """
        if title_id != None:
            titles = TitleVideoModel.objects.filter(id=title_id)
        else:
            titles = TitleVideoModel.objects.filter(
                video__Account__author=request.user)
        serializer = TitlesSerializer(titles, many=True)
        return Response(serializer.data)

    def post(self, request):
        """ Adding a new title. """
        serializer = TitlesSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            data = serializer.data
            title = data.get('title')
            #print( request.data.get('video_id') )
            videoid = request.data.get('video_id')
            publicato = bool(data.get('publicato', False))
            youtube_url = data.get('youtube_url', None)
            author = data.get('author', None)
            created_at = data.get('created_at', datetime.now())
            updated_at = data.get('updated_at', datetime.now())

            try:
                video = VideosModel.objects.get(pk=videoid)

                t = TitleVideoModel(title=title,
                                    video_id=video.id,
                                    publicato=publicato,
                                    youtube_url=youtube_url,
                                    author=author,
                                    created_at=created_at,
                                    updated_at=updated_at)
                t.save()
                request.data['id'] = t.pk  # return id
                return Response(request.data, status=status.HTTP_201_CREATED)
            except VideosModel.DoesNotExist as error:
                print(error)
                return Response(status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, title_id):
        """ Update a title """
        serializer = TitlesSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            data = serializer.data
            title = data.get('title')
            video_id = request.data.get('video_id')
            publicato = bool(data.get('publicato', False))
            youtube_url = data.get('youtube_url', None)
            author = data.get('author', None)  # TODO: must be the real author.
            created_at = data.get('created_at', datetime.now())
            updated_at = data.get('updated_at', datetime.now())

            try:
                video = VideosModel.objects.get(pk=video_id)

                t = TitleVideoModel(id=title_id,
                                    title=title,
                                    video_id=video.id,
                                    publicato=publicato,
                                    youtube_url=youtube_url,
                                    author=author,
                                    created_at=created_at,
                                    updated_at=updated_at)
                t.save()
                return Response(status=status.HTTP_200_OK)
            except VideosModel.DoesNotExist as error:
                print(error)
                return Response(status=status.HTTP_400_BAD_REQUEST)


class TitlesVideoRelatedView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, video_id):
        titles = TitleVideoModel.objects.filter(
            video__id=video_id,
            video__Account__author=request.user)
        serializer = TitlesSerializer(titles, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class TitlesSuggestView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, video_id):
        print(video_id)
        """ Get all titles for current video """

        try:
            video = VideosModel.objects.get(pk=video_id)

        except VideosModel.DoesNotExist as e:
            Response(status=status.HTTP_404_NOT_FOUND)

        try:
            titles = get_instant_suggestions(video.Title)
            if len(titles) == 0:
                Response(
                    {"message": "No titles found"},
                    status=status.HTTP_400_BAD_REQUEST)

            #suggestions = [{"title": title} for title in titles]
            suggestions = [title for title in titles]

            video.title_gen = True
            video.save()
            return Response(suggestions, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                {"message": "Error occured"},
                status=status.HTTP_400_BAD_REQUEST)


    def post(self, request, video_id):
        try:
            video = VideosModel.objects.get(
                pk=video_id,
                Account__author=request.user)
        except VideosModel.DoesNotExist as e:
             return Response(
                    {"message": "No video found"},
                    status=status.HTTP_404_NOT_FOUND)

        titles = request.POST.get('titles', "")

        for title in titles.split('\n'):
            if title:

                t = TitleVideoModel(
                    title=title,
                    video_id=video.id,
                    publicato=False,
                )
                t.save()
        return Response(
            {"message": "{} titles are created".format(len(titles.split('\n')))},
            status=status.HTTP_201_CREATED
        )

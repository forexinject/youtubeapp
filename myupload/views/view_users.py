# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from videoyoutube.settings import DEBUG
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.auth.models import User
from django.utils import six

from rest_framework_simplejwt.views import TokenObtainPairView

from myupload.serializers import (
    RegistrationSerializer,
    MyTokenObtainPairSerializer
)


class TokenGenerator(PasswordResetTokenGenerator):

    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class RegistrationAPIView(APIView):
    # Allow any for registration.
    permission_classes = (AllowAny,)

    # for test purpose only
    # permission_classes = (IsAuthenticated,)
    serializer_class = RegistrationSerializer

    def post(self, request):

        user = request.data

        # The create serializer, validate serializer, save serializer pattern
        # below is common and you will see it a lot throughout this course and
        # your own work later on. Get familiar with it.
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)

        try:
            user = User.objects.get(email=user['email'])
            return Response(
                {"email": ["A user with that email already exists."]},
                status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            pass

        u = serializer.save()

        if DEBUG == False:
            u.is_active = False
        u.save()

        account_activation_token = TokenGenerator()

        token = account_activation_token.make_token(u)
        uidb64 = urlsafe_base64_encode(force_bytes(u.id))

        # send email

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ConfirmRegistrationAPIView(APIView):
    permission_classes = (AllowAny,)
    account_activation_token = TokenGenerator()

    def get(self, request, uidb64, token):

        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and self.account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            return Response(
                {"message": "Success"},
                status=status.HTTP_200_OK)
        else:
            return Response(
                {"error": "Wrong activation token"},
                status=status.HTTP_404_NOT_FOUND
            )

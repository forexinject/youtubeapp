import os
import logging

from django.urls import reverse
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from videoyoutube.celery import app
#from youtube_upload.main import upload_video
import json
import datetime

from  myupload.googleapis import CeleryInstalledAppFlow

from django.db.models import Q

from myupload.models import (
    AccountsModel,
    VideosModel,
    TitleVideoModel
)

from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

CHANNEL_TIME_INTERVAL = 10


@app.task
def upload_video_task():
    now = datetime.datetime.now()
    logger.info("start uploading... {}".format(
        datetime.datetime.now().timestamp()))

    UserModel = get_user_model()
    try:

        #from myupload.tasks import upload_video_task
        # upload_video_task.delay()

        videos = TitleVideoModel.objects.filter(
            updated_at__lt=(
                now - datetime.timedelta(minutes=CHANNEL_TIME_INTERVAL)),
            publicato=False,
            pending=False)

        for video in videos:
            pass

        print(schedule)
    except Exception as e:
        logging.warning(
            "Error occured inside celery video uploading task.")

# Google api auth task
#
#
#
#

SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

@app.task(ignore_result=True)
def get_token(secret, account_id):
    account = AccountsModel.all_objects.get(pk=account_id)
    # main flow
    flow = CeleryInstalledAppFlow.from_client_secrets_file(secret, SCOPES)
    flow.set_account(account)

    flow.run_as_celery()
    credentials = flow.credentials

    print(dir(credentials))

    # request_token = {
    #     "access_token": credentials.token,
    #     "token_type":"Bearer",
    #     "refresh_token":credentials.refresh_token,
    #     "client_secret": credentials.client_secret,
    #     "client_id": credentials.client_id,
    #     "_module": "oauth2client.client",
    #     "_class": "OAuth2Credentials",
    #     "expiry": str(credentials.expiry)
    # }

    request_token = {
        "revoke_uri": "https://accounts.google.com/o/oauth2/revoke",
        "access_token": credentials.token,
        "client_secret": credentials.client_secret,
        "token_uri": "https://www.googleapis.com/oauth2/v3/token",
        "token_expiry": str(credentials.expiry), #"2018-09-04T00:01:53Z",
        "client_id": credentials.client_id,
        "id_token_jwt": None,
        "_module": "oauth2client.client",
        "scopes": ["https://www.googleapis.com/auth/youtube.upload"],
        "refresh_token": credentials.refresh_token,
        "token_info_uri": "https://www.googleapis.com/oauth2/v3/tokeninfo",
        "user_agent": None,
        "_class": "OAuth2Credentials",
        "invalid": False,
        "id_token": None,
        "token_response": {
            "token_type": "Bearer",
            "access_token": credentials.token,
            "scope": "https://www.googleapis.com/auth/youtube.upload",
            "refresh_token": credentials.refresh_token,
            "expires_in": 3600
        }
    }

    token_path = os.path.dirname(account.ClientSecrets.path)
    unique_uuid = str(account.ClientSecrets).split('/')[1]
    token_request = "{}/token.{}.request".format(token_path, account.id)

    with open(token_request, 'w') as token:
        token.write(json.dumps(request_token))
    account.RequestToken = "accounts/{}/token.{}.request".format(unique_uuid, account.id)
    account.save()
    return token_request


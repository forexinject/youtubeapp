# posts/serializers.py
from django.contrib.auth.models import User
from rest_framework import serializers
from django.db import IntegrityError
from . import models

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(MyTokenObtainPairSerializer, cls).get_token(user)

        # Add custom claims
        token['full_name'] = "{} {}".format(user.last_name, user.first_name)
        token['username'] = "{}".format(user.username)

        return token


class AccountsSerializer(serializers.ModelSerializer):

    author = serializers.SerializerMethodField()
    total_videos = serializers.SerializerMethodField()

    class Meta:
        fields = ('id', 'ChannelName', 'ChannelURL', 'ClientSecrets', 'RequestToken',
                  'Note', 'counter', 'author', 'created_at', 'updated_at',
                  'total_videos')
        model = models.AccountsModel

    def get_author(self, obj):
        return obj.author.username


    def get_total_videos(self, obj):
        return obj.accounts.count()


class VideosSerializer(serializers.ModelSerializer):

    total_titles = serializers.SerializerMethodField()
    author = serializers.SerializerMethodField()
    account_title = serializers.SerializerMethodField()

    class Meta:
        fields = ('id', 'Title', 'Description', 'Tags', 'Category', 'Language',
                  'file', 'Playlist', 'Account', 'account_title', 'created_at', 
                  'updated_at', 'total_titles', 'author')
        model = models.VideosModel


    def get_author(self, obj):
        return obj.Account.author.username

    def get_account_title(self, obj):
        return obj.Account.ChannelName

    def get_total_titles(self, obj):
        return obj.video_id.count() # video_id = related name for titles


class VideoUploadSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'Title', 'Description', 'Tags', 'Category', 'Language',
                  'file', 'Playlist', 'Account')
        model = models.VideosModel


class TitlesSerializer(serializers.ModelSerializer):

    youtube_id = serializers.SerializerMethodField()

    class Meta:
        fields = ('id', 'title', 'video_id', 'publicato', 'pending',
                  'youtube_url', 'youtube_id', 'author', 'created_at', 'updated_at')
        model = models.TitleVideoModel


    def get_youtube_id(self, obj):
        """
        https://www.youtube.com/watch?v=w3XC7XVE3IM
        """
        return obj.youtube_url.replace(
            "https://www.youtube.com/watch?v=", "")


class VideoCategorySerializer(serializers.Serializer):

    id = serializers.CharField()
    title = serializers.CharField()


class LanguageSerializer(serializers.Serializer):

    id = serializers.CharField()
    title = serializers.CharField()


class RegistrationSerializer(serializers.ModelSerializer):
    """Serializers registration requests and creates a new user."""

    # Ensure passwords are at least 8 characters long, no longer than 128
    # characters, and can not be read by the client.
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )

    class Meta:
        model = User

        fields = ['email', 'password', 'username']

    def create(self, validated_data):
        #validated_data['username'] = validated_data['email']

        user = None

        try:
            user = User.objects.create_user(**validated_data)
        except IntegrityError as e:
            # TODO: exception for duplicate value violate
            # print("Error")
            pass
        return user


class BashboardSerializer(serializers.Serializer):

    new_channels = serializers.SerializerMethodField()
    new_videos = serializers.SerializerMethodField()

    class Meta:
        model = models.AccountsModel

        fields = ("author", "new_channels", 'new_videos')

    def get_new_channels(self, obj):
        return self.new_channels

    def get_new_videos(self, obj):
            return self.new_videos


class AccountStep2Serializer(serializers.Serializer):

    class Meta:
        model = models.AccountsModel
        fields = ("id", 'code')
# posts/urls.py
from django.conf.urls import url, include
from django.urls import path, re_path
from rest_framework.urlpatterns import format_suffix_patterns

from .views.view_titles import (
    TitlesView,
    TitleConversion,
    TitleAsDone,
    TitlesSuggestView,
    TitlesVideoRelatedView
)
from .views.view_accounts import AccountsList, AccountDetail, DashboardAPIView, AccountStep2Detail
from .views.view_videos import VideosList, VideoDetail
from .views.view_videos import (
    VideosList,
    VideoDetail,
    VideoUploadView,
    VideoCategoryList,
    VideoChannelView,
    LanguageList)


urlpatterns = [

    #---- accounts

    path('accounts', AccountsList.as_view(), name="accounts-list"),
    path('accounts/<int:pk>', AccountDetail.as_view(), name="account_details"),
    path('account/channels', AccountDetail.as_view(), name="account_channels"),
    path('account', AccountDetail.as_view(), name="account_create"),
    path('account/<int:pk>/code', AccountStep2Detail.as_view(), name="account_2_create"),
    path('account/<int:pk>', AccountDetail.as_view(), name="account_delete"),

    #---- videos

    path('videos', VideoUploadView.as_view(), name="videos_list"),
    path('videos/channel/<int:channel_id>', VideoChannelView.as_view(), name="videos_list_channel"),
    path('videos/<int:pk>', VideoUploadView.as_view(), name="video_details"),
    #path('videos/<int:pk>', VideoDetail.as_view(), name="video_action"),

    #---- title

    path('titles', TitlesView.as_view(), name="accounts_list"),
    path('titles/<int:title_id>', TitlesView.as_view(), name="title_details"),
    path('titles/suggest/<int:video_id>', TitlesSuggestView.as_view(), name="title_get"),
    path('titles/video/<int:video_id>', TitlesVideoRelatedView.as_view(), name="title_get"),
    path('titles/video/get', TitleConversion.as_view(), name="title_get"),
    path('titles/video/done', TitleAsDone.as_view(), name="title_mark_as_done"),
    path('titles/suggest/<int:video_id>', TitlesSuggestView.as_view(), name="title_suggest"),


    # references

    path('references/video/categories',
        VideoCategoryList.as_view(), name="reference_video_categories"),
    path('references/languages',
        LanguageList.as_view(), name="reference_languages"),

    path('dashboard/<str:period>', DashboardAPIView.as_view(), name="dashboard"),
]


urlpatterns = format_suffix_patterns(urlpatterns)

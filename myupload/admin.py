# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
#from djcoin.app.models import AccountsModel
#from djcoin.app.models import VideosModel
from .models import AccountsModel, VideosModel, TitleVideoModel

class AccountsAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'Note', 'author', 'created_at', 'counter', 'deleted')
    list_filter = ['created_at', 'author', 'deleted']
    search_fields = ['ChannelName', 'AuthUri']
    readonly_fields = ['counter']
    exclude = ['AuthUri','ClientSecret','ClientID']

    class Meta:
        model = AccountsModel

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()

    def get_queryset(self, request):
        return self.model.all_objects.all()

admin.site.register(AccountsModel, AccountsAdmin)


def generate_instant_title(self, modeladmin, request, queryset):
    queryset.update(title_gen=True)
    generate_instant_title.short_description = "gen instant suggestion title"
    #todo: fix this


class VideosAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'Account', 'author', 'created_at', 'title_gen', 'deleted')
    list_filter = ['created_at', 'author', 'deleted']
    search_fields = ['Title', 'Description', 'Tags', 'Playlist']
    #readonly_fields = ['author','title_gen']
    #exclude = ['title_gen']
    #actions = [generate_instant_title]

    class Meta:
        model = VideosModel


    def get_queryset(self, request):
        return self.model.all_objects.all()

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()

admin.site.register(VideosModel, VideosAdmin)


class TitleAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'video_id', 'youtube_url', 'publicato', 'author')
    list_filter = ['created_at', 'publicato', 'author']
    search_fields = ['title', 'ip_address', 'youtube_url', 'author']
    #readonly_fields = ['youtube_url', 'publicato', 'ip_address', 'author', 'title', 'video', 'video_id', 'author']

    class Meta:
        model = TitleVideoModel

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()
admin.site.register(TitleVideoModel, TitleAdmin)

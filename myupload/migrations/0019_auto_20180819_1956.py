# Generated by Django 2.1 on 2018-08-19 19:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myupload', '0018_auto_20180819_1610'),
    ]

    operations = [
        migrations.AddField(
            model_name='titlevideomodel',
            name='pending',
            field=models.BooleanField(default=False, help_text='video is pengind processing'),
        ),
        migrations.AlterField(
            model_name='videosmodel',
            name='Category',
            field=models.CharField(choices=[('2', 'Autos & Vehicles'), ('1', 'Film & Animation'), ('10', 'Music'), ('15', 'Pets & Animals'), ('17', 'Sports'), ('18', 'Short Movies'), ('19', 'Travel & Events'), ('20', 'Gaming'), ('21', 'Videoblogging'), ('22', 'People & Blogs'), ('23', 'Comedy'), ('24', 'Entertainment'), ('25', 'News & Politics'), ('26', 'Howto & Style'), ('27', 'Education'), ('28', 'Science & Technology'), ('29', 'Nonprofits & Activism'), ('30', 'Movies'), ('31', 'Anime/Animation'), ('32', 'Action/Adventure'), ('33', 'Classics'), ('34', 'Comedy'), ('35', 'Documentary'), ('36', 'Drama'), ('37', 'Family'), ('38', 'Foreign'), ('39', 'Horror'), ('40', 'Sci-Fi/Fantasy'), ('41', 'Thriller'), ('42', 'Shorts'), ('43', 'Shows'), ('44', 'Trailers')], default='28', max_length=32, verbose_name='Category'),
        ),
    ]

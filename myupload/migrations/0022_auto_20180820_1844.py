# Generated by Django 2.1 on 2018-08-20 18:44

from django.db import migrations, models
import myupload.models

def update_filename(instance, filename):
    return 'accounts/{}/{}'.format(instance.id, filename)


class Migration(migrations.Migration):

    dependencies = [
        ('myupload', '0021_auto_20180820_1453'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='accountsmodel',
            name='file',
        ),
        migrations.AddField(
            model_name='accountsmodel',
            name='ClientSecrets',
            field=models.FileField(default='', null=True, upload_to=myupload.models.update_filename),
        ),
        migrations.AddField(
            model_name='accountsmodel',
            name='RequestToken',
            field=models.FileField(default='', null=True, upload_to=myupload.models.update_filename),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-08-18 23:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myupload', '0003_videosmodel_account'),
    ]

    operations = [
        migrations.AddField(
            model_name='videosmodel',
            name='file',
            field=models.FileField(default='aici video', upload_to='video/'),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-08-18 22:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Accounts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ChannelName', models.CharField(max_length=100, unique=True)),
                ('AuthUri', models.CharField(max_length=250, unique=True)),
                ('ClientSecret', models.CharField(max_length=250, unique=True)),
                ('ClientID', models.CharField(max_length=250, unique=True)),
                ('Note', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Videos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Title', models.CharField(max_length=100, unique=True)),
                ('Description', models.CharField(blank=True, max_length=1024, null=True)),
                ('Tags', models.CharField(max_length=250, unique=True)),
                ('Category', models.CharField(choices=[(b'cat1', b'cat1'), (b'cat2', b'cat2')], default=b'cat1', max_length=32, verbose_name=b'Language')),
                ('Language', models.CharField(choices=[(b'EN', b'EN'), (b'IT', b'IT')], default=b'EN', max_length=32, verbose_name=b'Language')),
                ('Playlist', models.CharField(max_length=100, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]

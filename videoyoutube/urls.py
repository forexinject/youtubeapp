"""videoyoutube URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include, re_path
#from django.urls import path
from django.contrib import admin

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

from myupload.views.static_views import home
from myupload.views.view_users import (
    RegistrationAPIView,
    MyTokenObtainPairView,
    ConfirmRegistrationAPIView
)

from myupload.views.view_static import home


admin.site.site_header = 'YoutubeApp'

urlpatterns = [
    re_path(r'^$', home, name='home'),
    re_path(r'^admin/', admin.site.urls),
    re_path('api/', include('myupload.urls')),
    #re_path(r'^$', home, name='home'),   # redirect to admin panel


    # JWT autentificare - https://github.com/davesque/django-rest-framework-simplejwt
    # TODO: trebuie studiata aceasta librarie :
    # https://github.com/GetBlimp/django-rest-framework-jwt

    re_path(r'^auth/token/$',
        TokenObtainPairView.as_view(), name='token_obtain_pair'),
    re_path(r'^auth/signin/$',
        MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    re_path(r'^auth/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})$',
        ConfirmRegistrationAPIView.as_view(), name='confirm_registration'),
    re_path(r'^auth/token/refresh/$',
        TokenRefreshView.as_view(), name='token_refresh'),
    re_path(r'^auth/token/verify/$',
        TokenVerifyView.as_view(), name='token_verify'),

    re_path(r'^auth/signup/$',
        RegistrationAPIView.as_view(), name='registration'),

]
